# Pong Online #

### By Iván Matellanes and Asier Mujika ###

Pong Online is an implementation of the classic 'Pong' game allowing two players on different computers to play over the net. Its graphics are very simple because the goal of this project was to learn about network programming.

We defined a simple application protocol for the gameplay over UDP, and used the BSD socket interface for the implementation. For the game graphics, basic OpenGL was used.


# Pong Online #

### Por Iván Matellanes y Asier Mujika ###

Pong Online es una implementación del clásico juego 'Pong' que permite a dos jugadores en distintos ordenadores jugar una partida a través de la red. Los gráficos son muy sencillos puesto que el objetivo de este proyecto era aprender sobre programación de redes.

Definimos un protocolo de aplicación sencillo sobre UDP, y utilizamos la interfaz de sockets BSD para la implementación. Para los gráficos del juego usamos OpenGL básico.
