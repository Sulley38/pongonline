#include <GL/freeglut.h>
#include <GL/glut.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <netinet/in.h>
#include <netdb.h>
#include <sys/socket.h>
#include <sys/types.h>

#include "pongonline.h"

// Variables globales del cliente
int sock, p, pos_jugador[2] = {0, 0}, pos_bola[2] = {0, 0}, puntos[2] = {0,0};
int ultimo_id = -1;
char marcador[10];

// Declaración de funciones del cliente
void comenzar_partida(int argc, char** argv);
void dibujar();
void teclas(int key, int x, int y);
void leer_comando();


int main(int argc, char** argv) {
    
    // Comprobar que los argumentos de llamada son correctos
    if (argc != 2) {
        printf("Uso: %s <Direccion IPv4 o nombre del servidor>\n", argv[0]);
        exit(EXIT_FAILURE);
    }
    
    // Variables para gestionar la conexión
    int n, retryCounter = 0;
    char buf[MAX_BUF];
    struct sockaddr_in dir_serv;
    struct hostent *hp;
    struct timeval time;
    socklen_t tam_dir;
    fd_set readfds;

    // Crear un socket.
    if ((sock = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
        error("Error al crear el socket");

    // Rellenar la estructura de dirección del servidor
    memset(&dir_serv, 0, sizeof(dir_serv));
    dir_serv.sin_family = AF_INET;
    dir_serv.sin_port = htons(PORT);
    if((hp = gethostbyname(argv[1])) == NULL)
        error("Error al resolver el nombre del servidor");
    memcpy(&dir_serv.sin_addr, hp->h_addr, hp->h_length);

    /*
     * Enviar peticion de partida hasta recibir respuesta
     * o haber hecho suficientes intentos
     */
    setbuf(stdout, NULL);
    printf("Buscando partida..");
    do {
        printf(".");
        if (sendto(sock, "GAME", strlen("GAME"), 0, (struct sockaddr *) &dir_serv, sizeof (dir_serv)) < 0)
            error("Error al enviar petición de partida");

        // Establecer socket y tiempo máximo de bloqueo
        FD_ZERO(&readfds);
        FD_SET(sock, &readfds);
        time.tv_sec = 0;
        time.tv_usec = GAME_RETRY;

        // Esperar respuesta
        select(sock + 1, &readfds, 0, 0, &time);
    } while(!FD_ISSET(sock, &readfds) && (retryCounter++ < MAX_GAME_ATTEMPTS));
    
    puts("");
    
    if (!FD_ISSET(sock, &readfds)) {
        puts("No se pudo conectar con el servidor o no se encontró partida, inténtelo más tarde");
        close(sock);
        exit(EXIT_FAILURE);
    }
    
    // Recibir el primer paquete de partida, guardando la nueva dirección del servidor
    memset(&dir_serv, 0, sizeof(dir_serv));
    tam_dir = sizeof (dir_serv);
    if ((n = recvfrom(sock, buf, MAX_BUF, 0, (struct sockaddr *) &dir_serv, &tam_dir)) < 0)
        error("Error al recibir datos");

    // Conectar el socket al servidor de partida
    if (connect(sock, (struct sockaddr *) &dir_serv, sizeof (dir_serv)) < 0)
        error("Error al intentar conectar con el servidor");

    // Comenzar la partida
    comenzar_partida(argc, argv);

    close(sock);
    return (EXIT_SUCCESS);
}

/*
 * Esta función se encarga de poner en marcha el sistema OpenGL (mediante la librería GLUT).
 * El control de la ejecución se le pasa a GLUT al llamar a glutMainLoop().
 * El resto de funciones se registran como callbacks a ciertos eventos:
 *      dibujar() -> evento de dibujar la ventana
 *      teclas(...) -> evento de tecla pulsada
 *      leer_estado() -> cuando no hay eventos gráficos pendientes de procesar
 */
void comenzar_partida(int argc, char** argv) {
    // Iniciar motor gráfico GLUT
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGB);
    glutInitWindowSize(WINDOW_WIDTH, WINDOW_HEIGHT);
    glutInitWindowPosition(100, 100);
    glutCreateWindow("PongOnline");

    // Establecer parámetros de visualización
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, WINDOW_WIDTH, 0, WINDOW_HEIGHT, -10, 10);
    glMatrixMode(GL_MODELVIEW);
    
    // Indicar funciones callback para responder a los eventos
    glutDisplayFunc(dibujar);
    glutSpecialFunc(teclas);
    glutIdleFunc(leer_comando);

    // Indicar color de fondo y comportamiento al cerrar la ventana
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);
    
    // Comenzar bucle principal del juego
    glutMainLoop();
}

/*
 * Función para dibujar en pantalla los elementos del juego: palas, líneas, bola y marcador.
 */
void dibujar() {
    glClear(GL_COLOR_BUFFER_BIT);
    
    // Dibujar las líneas que delimitan el campo
    glColor3f(1.0f, 0.0f, 0.0f); // Color rojo
    glLineWidth(3);
    glBegin(GL_LINES);
        // Línea izquierda
        glVertex2i(GOAL_LINE, 0);
        glVertex2i(GOAL_LINE, WINDOW_HEIGHT);
        // Línea derecha
        glVertex2i(WINDOW_WIDTH - GOAL_LINE, 0);
        glVertex2i(WINDOW_WIDTH - GOAL_LINE, WINDOW_HEIGHT);
    glEnd(); 
    
    // Dibujar pala del jugador 1
    glColor3f(1.0f, 1.0f, 1.0f); // Color blanco
    glRecti(PADDLE1_X - PADDLE_WIDTH, WINDOW_HEIGHT - pos_jugador[0],
            PADDLE1_X, WINDOW_HEIGHT - (pos_jugador[0] + PADDLE_HEIGHT));
    
    // Dibujar pala del jugador 2
    glRecti(PADDLE2_X, WINDOW_HEIGHT - pos_jugador[1],
            PADDLE2_X + PADDLE_WIDTH, WINDOW_HEIGHT - (pos_jugador[1] + PADDLE_HEIGHT));
    
    // Dibujar la bola
    glRecti(pos_bola[0], WINDOW_HEIGHT - pos_bola[1],
            pos_bola[0] + BALL_SIDE, WINDOW_HEIGHT - (pos_bola[1] + BALL_SIDE));
    
    // Mostrar el marcador con la puntuación
    snprintf(marcador, 10, "%d - %d", puntos[0], puntos[1]);
    glRasterPos2i(WINDOW_WIDTH / 2, WINDOW_HEIGHT - GOAL_LINE);
    glutBitmapString(GLUT_BITMAP_HELVETICA_18, (const unsigned char *)marcador);
    
    // Mostrar la nueva situación en pantalla
    glFlush();
}

/*
 * Función para gestionar las teclas pulsadas.
 * En nuestro caso, solo importan las flechas arriba y abajo.
 */
void teclas(int key, int x, int y) {
    switch (key) {
	case GLUT_KEY_UP:
            // Pedir al servidor que suba la pala del jugador
	    write(sock, "MOVE U", strlen("MOVE U"));
	    break;
	case GLUT_KEY_DOWN:
            // Pedir al servidor que baje la pala del jugador
	    write(sock, "MOVE D", strlen("MOVE D"));
	    break;
    }
}

/*
 * Esta función espera un comando del servidor (STATE o END) y lo procesa adecuadamente.
 */
void leer_comando() {
    char buf[MAX_BUF];
    int id, n;
    
    // Leer comando del servidor
    if ((n = read(sock, buf, MAX_BUF - 1)) < 0)
        error("Error en la comunicación con el servidor");
    buf[n] = 0;
    
    // Si el servidor indica que la partida ha finalizado, enviar confirmación y terminar
    if (strncmp(buf, "END", 3) == 0) {
        write(sock, "OK", strlen("OK"));
        if(buf[4] == 'W') // Ganador
            printf("¡Enhorabuena! Has ganado la partida\n");
        else // Perdedor
            printf("¡Has perdido! Buena suerte la próxima vez\n");
        glutLeaveMainLoop(); // Pedir de vuelta el control de la ejecución
    } else {
        // Leer los parámetros del comando STATE
        sscanf(buf, "STATE %d", &id);
        if (id > ultimo_id)
            sscanf(buf, "STATE %d %d %d %d %d %d %d", &ultimo_id, &pos_jugador[0],
                    &pos_jugador[1], &pos_bola[0], &pos_bola[1], &puntos[0], &puntos[1]);
        // Pedir a GLUT que vuelva a dibujar la pantalla
        glutPostRedisplay();
    }
}
