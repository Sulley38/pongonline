#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Cliente configuration
CND_PLATFORM_Cliente=GNU-Linux-x86
CND_ARTIFACT_DIR_Cliente=dist
CND_ARTIFACT_NAME_Cliente=pongonline_cli
CND_ARTIFACT_PATH_Cliente=dist/pongonline_cli
CND_PACKAGE_DIR_Cliente=dist/Cliente/GNU-Linux-x86/package
CND_PACKAGE_NAME_Cliente=pongonline.tar
CND_PACKAGE_PATH_Cliente=dist/Cliente/GNU-Linux-x86/package/pongonline.tar
# Servidor configuration
CND_PLATFORM_Servidor=GNU-Linux-x86
CND_ARTIFACT_DIR_Servidor=dist
CND_ARTIFACT_NAME_Servidor=pongonline_ser
CND_ARTIFACT_PATH_Servidor=dist/pongonline_ser
CND_PACKAGE_DIR_Servidor=dist/Servidor/GNU-Linux-x86/package
CND_PACKAGE_NAME_Servidor=pongonline.tar
CND_PACKAGE_PATH_Servidor=dist/Servidor/GNU-Linux-x86/package/pongonline.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
