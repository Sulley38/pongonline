#ifndef PONGONLINE_H
#define	PONGONLINE_H

#define trace( ... ) fprintf(stdout, __VA_ARGS__);
#define error(s) { perror(s); exit(EXIT_FAILURE); }
#define max(a, b) ((a > b) ? a : b)

/* Parámetros de la partida */

#define WINDOW_WIDTH 800
#define WINDOW_HEIGHT 600

#define BALL_SIDE 40
#define PADDLE_WIDTH 25
#define PADDLE_HEIGHT 120
#define PADDLE_SHIFT 5 // Desplazamiento de la barra de un jugador en el eje Y
#define PADDLE1_X 75 // Posición del lado derecho de la barra del jugador 1
#define PADDLE2_X 725 // Posición del lado izquierdo de la barra del jugador 2
#define GOAL_LINE 30 // Distancia de la línea de tanto al borde de la pantalla

/* Comandos/respuestas que puede recibir el servidor conectado */

#define COM_MOVE_UP 1
#define COM_MOVE_DOWN 2
#define COM_OK 3

/* Parámetros de conexión */

#define MAX_BUF 1024
#define PORT 13337

#define GAME_RETRY 500000 // Tiempo de espera (microsegundos) hasta volver a enviar GAME
#define MAX_GAME_ATTEMPTS 120 // Número máximo de intentos de envío del comando GAME
#define WAITLIST_RESET 1 // Intervalo (segundos) entre reinicios de la lista de espera del servidor
#define STATE_RATE 20000 // Intervalo (microsegundos) entre dos envíos de comando STATE
#define END_RETRY 500000 // Tiempo de espera (microsegundos) hasta volver a enviar END
#define MAX_END_ATTEMPTS 15 // Número máximo de intentos de envío del comando END

//#define PACKET_LOSS // Descomentar si se quiere simular la pérdida de paquetes STATE
#define PERCENT_LOST 20 // Porcentaje de paquetes STATE que se pierden

struct GameBall {
    int x, y;
    double vx, vy;
};

#endif	/* PONGONLINE_H */

