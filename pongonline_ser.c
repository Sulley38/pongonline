#include <math.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>

#include "pongonline.h"

// Declaración de funciones del servidor
inline int mismo_jugador(struct sockaddr_in *dir1, struct sockaddr_in *dir2);
void reiniciar_lista(int *num_jugadores, struct sockaddr_in *lista, struct timeval *temporizador);
void partida(int jugador[2], const int dificultad, const int puntuacion_objetivo);
int procesar_comando(int sock);
void iniciar_bola(const int dificultad, struct GameBall *bola);
inline int colision_jugador1(struct GameBall *bola, const int *pos_barra);
inline int colision_jugador2(struct GameBall *bola, const int *pos_barra);
void fin_partida(int s_jugador[2], const int ganador, const int perdedor);


int main(int argc, char** argv) {

    // Variables de partida
    int dificultad, puntuacion, num_jugadores = 0;
    
    // Comprobar que los argumentos de llamada son correctos
    if (argc != 3) {
        printf("Uso: %s <Dificultad> <Puntuacion objetivo>\n", argv[0]);
        printf("La dificultad debe ser un valor entero entre 1 y 10.\n");
        printf("La puntuación objetivo debe ser un valor entero entre 1 y 50.\n");
        exit(EXIT_FAILURE);
    } else if ((dificultad = atoi(argv[1])) < 1 || dificultad > 10) {
        printf("La dificultad debe ser un valor entero entre 1 y 10.\n");
        exit(EXIT_FAILURE);
    } else if ((puntuacion = atoi(argv[2])) < 1 || puntuacion > 50) {
        printf("La puntuación objetivo debe ser un valor entero entre 1 y 50.\n");
        exit(EXIT_FAILURE);
    }
    
    // Variables para gestionar la conexión
    int sock_serv, n;
    char buf[MAX_BUF];
    struct sockaddr_in dir_serv, lista_espera[2];
    struct timeval timer;
    socklen_t tam_dir[2];
    fd_set rset;
    
    // Crear el socket
    if ((sock_serv = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
	error("Error al crear el socket");
    
    // Rellenar los campos de dirección del socket
    memset(&dir_serv, 0, sizeof(dir_serv));
    dir_serv.sin_family = AF_INET;
    dir_serv.sin_addr.s_addr = htonl(INADDR_ANY);
    dir_serv.sin_port = htons(PORT);
    
    // Asignar la dirección al socket
    if (bind(sock_serv, (struct sockaddr *) &dir_serv, sizeof(dir_serv)) < 0)
        error("Error al asignar una direccion al socket");
    
    // Ignorar la señal de fin de proceso hijo para evitar zombies
    signal(SIGCHLD, SIG_IGN);
    
    // Inicializar la lista de espera
    reiniciar_lista(&num_jugadores, lista_espera, &timer);
    
    while (1) {
        
        FD_ZERO(&rset);
        FD_SET(sock_serv, &rset);
        
        // Esperar peticiones de partida o el momento de reiniciar la lista de espera 
        if (select(sock_serv + 1, &rset, 0, 0, &timer) > 0) {
            /* Recibir comando de un cliente */
            tam_dir[num_jugadores] = sizeof(lista_espera[num_jugadores]);
            if ((n = recvfrom(sock_serv, buf, MAX_BUF - 1, 0,
                     (struct sockaddr *) &lista_espera[num_jugadores], &tam_dir[num_jugadores])) < 0)
                error("Error al recibir datos");
            buf[n] = 0;
            trace("Recibido: %s\n", buf);

            if (strncmp(buf, "GAME", n) == 0) {
                // En caso de recibir un GAME del jugador que ya está en la lista de espera,
                // se ignora el comando y se sigue esperando
                if (num_jugadores == 1 && !mismo_jugador(&lista_espera[0], &lista_espera[1])) {
                    // Crear proceso hijo para lanzar la partida
                    switch(fork()) {
                    case -1:
                        error("Error al crear el proceso hijo");
                    case 0:
                        close(sock_serv);
                        
                        // Crear dos nuevos sockets para el transcurso de la partida
                        int sock_jugador[2];
                        if ((sock_jugador[0] = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ||
                            (sock_jugador[1] = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
                            error("Error al crear un socket para la partida");
                        
                        // Conectar los sockets con los respectivos clientes
                        if (connect(sock_jugador[0], (struct sockaddr *) &lista_espera[0], tam_dir[0]) < 0 ||
                            connect(sock_jugador[1], (struct sockaddr *) &lista_espera[1], tam_dir[1]) < 0)
                            error("Error al conectar con los clientes para iniciar la partida");
                        
                        // Comenzar el desarrollo del juego
                        partida(sock_jugador, dificultad, puntuacion);
                        
                        // Terminar la ejecución
                        close(sock_jugador[0]);
                        close(sock_jugador[1]);
                        exit(EXIT_SUCCESS);
                    default:
                        // El servidor padre espera nuevos jugadores
                        reiniciar_lista(&num_jugadores, lista_espera, &timer);
                    }
                } else {
                    // Esperar a otro jugador
                    num_jugadores = 1;
                }
                
            } else {
                trace("Comando inesperado: %s\n", buf);
            }
            
        } else {
            /* Reiniciar la lista de espera */
            reiniciar_lista(&num_jugadores, lista_espera, &timer);
        }
        
    }
    
    // Nunca se debería llegar aquí
    close(sock_serv);
    return (EXIT_SUCCESS);
}

/*
 * Comprobar si dir1 equivale al mismo proceso que dir2, esto es,
 * si su dirección IP y número de puerto coinciden.
 */
inline int mismo_jugador(struct sockaddr_in *dir1, struct sockaddr_in *dir2) {
    return (dir1->sin_addr.s_addr == dir2->sin_addr.s_addr && dir1->sin_port == dir2->sin_port);
}

/*
 * Reinicia la lista de espera, estableciendo el número de clientes que quieren jugar a 0
 * y reseteando el contador para el próximo reinicio de la lista.
 */
void reiniciar_lista(int *num_jugadores, struct sockaddr_in *lista, struct timeval *temporizador) {
    *num_jugadores = 0;
    memset(&lista[0], 0, sizeof(lista[0]));
    memset(&lista[1], 0, sizeof(lista[1]));
    temporizador->tv_sec = WAITLIST_RESET; // Tiempo hasta el próximo reinicio de la lista de espera
    temporizador->tv_usec = 0;
}

/*
 * Control de la partida y lógica del juego.
 * Los sockets identificados en s_jugador[i] se utilizan para comunicarse con los jugadores.
 * Cada uno de los pasos que se dan está comentado.
 */
void partida(int s_jugador[2], const int dificultad, const int puntuacion_objetivo) {
    // Semilla para los números aleatorios
    srand(time(0));
    
    // Variables relacionadas con el juego
    int pos_barra[2] = {(WINDOW_HEIGHT / 2) - (PADDLE_HEIGHT / 2), (WINDOW_HEIGHT / 2) - (PADDLE_HEIGHT / 2)};
    int puntuacion[2] = {0, 0};
    struct GameBall bola;
    iniciar_bola(dificultad, &bola);
    
    // Variables relacionadas con la comunicación
    int n, comando, state_id = 0, i;
    char buf[MAX_BUF];
    struct timeval timer;
    fd_set rset;
    
    timer.tv_sec = 0;
    timer.tv_usec = STATE_RATE; // Tiempo hasta el próximo STATE
    
    // Partida en curso hasta que un jugador alcance la puntuación objetivo
    while (puntuacion[0] < puntuacion_objetivo && puntuacion[1] < puntuacion_objetivo) {
        
        FD_ZERO(&rset);
        FD_SET(s_jugador[0], &rset);
        FD_SET(s_jugador[1], &rset);
        
        // Bloquearse hasta recibir un MOVE o tener que enviar un STATE
        if (select(max(s_jugador[0], s_jugador[1]) + 1, &rset, 0, 0, &timer) > 0) {
            /* Procesar un comando recibido */
            for (i = 0; i < 2; ++i) {
                if (FD_ISSET(s_jugador[i], &rset)) {
                    comando = procesar_comando(s_jugador[i]);
                    if (comando == -1) {
                        // No se puede comunicar con el jugador: victoria del otro jugador
                        puntuacion[abs(i-1)] = puntuacion_objetivo;
                        i = 2;
                    } else if (comando == COM_MOVE_UP) {
                        // Mover la barra hacia arriba (si no se llega al límite de la pantalla)
                        pos_barra[i] -= PADDLE_SHIFT;
                        if (pos_barra[i] < 0)
                            pos_barra[i] = 0;
                    } else if (comando == COM_MOVE_DOWN) {
                        // Mover la barra hacia abajo (si no se llega al límite de la pantalla)
                        pos_barra[i] += PADDLE_SHIFT;
                        if (pos_barra[i] >= WINDOW_HEIGHT - PADDLE_HEIGHT)
                            pos_barra[i] = WINDOW_HEIGHT - PADDLE_HEIGHT - 1;
                    }
                }
            }
        } else {
            /* Hora de enviar un nuevo STATE */
            timer.tv_sec = 0;
            timer.tv_usec = STATE_RATE;
        
            // Mover la bola
            bola.x += round(bola.vx);
            bola.y += round(bola.vy);

            // Evitar que la bola salga de la pantalla por arriba y abajo
            if (bola.y < 0) {
                bola.y = 0;
                bola.vy *= -1;
            } else if (bola.y + BALL_SIDE >= WINDOW_HEIGHT) {
                bola.y = WINDOW_HEIGHT - BALL_SIDE;
                bola.vy *= -1;
            }

            // Comprobar situación de la bola
            if (colision_jugador1(&bola, &pos_barra[0])) {
                // Choque con la barra del jugador 1
                bola.x = PADDLE1_X;
                bola.vx *= -1;
            } else if (colision_jugador2(&bola, &pos_barra[1])) {
                // Choque con la barra del jugador 2
                bola.x = PADDLE2_X - BALL_SIDE;
                bola.vx *= -1;
            } else if (bola.x + BALL_SIDE >= WINDOW_WIDTH - GOAL_LINE) {
                // Punto para el jugador 1
                iniciar_bola(dificultad, &bola);
                puntuacion[0]++;
                if (puntuacion[0] > puntuacion[1]) // Bola hacia el que va perdiendo
                    bola.vx *= -1;
            } else if (bola.x < GOAL_LINE) {
                // Punto para el jugador 2
                iniciar_bola(dificultad, &bola);
                puntuacion[1]++;
                if (puntuacion[0] > puntuacion[1]) // Bola hacia el que va perdiendo
                    bola.vx *= -1;
            }

            #ifdef PACKET_LOSS
            if (rand() % 100 > PERCENT_LOST) {
            #endif
            // Enviar el comando STATE a ambos jugadores
            n = snprintf(buf, MAX_BUF, "STATE %d %d %d %d %d %d %d", state_id++,
                    pos_barra[0], pos_barra[1], bola.x, bola.y, puntuacion[0], puntuacion[1]);
            if (write(s_jugador[0], buf, n) < n)
                perror("Error en la comunicación con el jugador 1");
            if (write(s_jugador[1], buf, n) < n)
                perror("Error en la comunicación con el jugador 2");
            #ifdef PACKET_LOSS
            }
            #endif
        }
        
    }
    
    // Fin de la partida
    if (puntuacion[0] == puntuacion_objetivo)
         // Victoria del jugador 1
        fin_partida(s_jugador, 0, 1);
    else
         // Victoria del jugador 2
        fin_partida(s_jugador, 1, 0);
}

/*
 * Lee el comando enviado por el jugador y, devuelve su identificador, o 0 si el comando es desconocido.
 * En otro caso, devuelve -1 si hay un error de comunciación.
 */
int procesar_comando(int sock) {
    int n;
    char buf[MAX_BUF];
    
    if ((n = read(sock, buf, MAX_BUF - 1)) > 0) {
        buf[n] = 0;
        if (strncmp(buf, "MOVE U", n) == 0) {
            return COM_MOVE_UP;
        } else if (strncmp(buf, "MOVE D", n) == 0) {
            return COM_MOVE_DOWN;
        } else if (strncmp(buf, "OK", n) == 0) {
            return COM_OK;
        } else {
            trace("Comando inesperado: %s\n", buf);
            return 0;
        }
    } else {
        // Posiblemente el jugador se haya desconectado
        trace("Error de lectura\n");
        return -1;
    }
}

/*
 * Sitúa la bola en el centro y le da un valor aleatorio a su dirección en base a la dificultad del juego.
 */
void iniciar_bola(const int dificultad, struct GameBall *bola) {
    double angulo = (rand() % 2593 - 1217) / 1000.0;
    bola->x = 400;
    bola->y = 300;
    bola->vx = dificultad * cos(angulo);
    bola->vy = dificultad * sin(angulo);
    // Evitar que la bola salga en horizontal
    if (bola->vy > 0)
        bola->vy += 0.5;
    else
        bola->vy -= 0.5;
}

/*
 * Comprueba si la bola choca con la barra del jugador 1.
 */
inline int colision_jugador1(struct GameBall *bola, const int *pos_barra) {
    return (bola->x <= PADDLE1_X && bola->x >= PADDLE1_X - PADDLE_WIDTH &&
            bola->y + BALL_SIDE >= *pos_barra && bola->y <= *pos_barra + PADDLE_HEIGHT);
}

/*
 * Comprueba si la bola choca con la barra del jugador 2.
 */
inline int colision_jugador2(struct GameBall *bola, const int *pos_barra) {
    return (bola->x + BALL_SIDE >= PADDLE2_X && bola->x + BALL_SIDE <= PADDLE2_X + PADDLE_WIDTH &&
            bola->y + BALL_SIDE >= *pos_barra && bola->y <= *pos_barra + PADDLE_HEIGHT);
}

/*
 * Termina la partida enviando los comandos END W/L a ganador y perdedor, respectivamente,
 * y espera a que ambos hayan confirmado o se superen MAX_END_ATTEMPTS intentos.
 */
void fin_partida(int s_jugador[2], const int ganador, const int perdedor) {
    int num_intentos = 0, recibido[2] = {0, 0}, max_sock, comando;
    struct timeval timer;
    fd_set rset;
    
    timer.tv_sec = 0;
    timer.tv_usec = 1; // Hacer el primer intento ahora mismo
    
    while (num_intentos < MAX_END_ATTEMPTS && (!recibido[ganador] || !recibido[perdedor])) {
        
        FD_ZERO(&rset);
        max_sock = 0;
        if (!recibido[ganador]) {
            FD_SET(s_jugador[ganador], &rset);
            max_sock = s_jugador[ganador];
        }
        if (!recibido[perdedor]) {
            FD_SET(s_jugador[perdedor], &rset);
            max_sock = max(max_sock, s_jugador[perdedor]);
        }
        
        // Bloquearse hasta recibir un OK o tener que enviar un END  
        if (select(max_sock + 1, &rset, 0, 0, &timer) > 0) {
            /* Procesar un comando recibido */
            // Comando proveniente del jugador ganador
            if (FD_ISSET(s_jugador[ganador], &rset))
                if ((comando = procesar_comando(s_jugador[ganador])) == COM_OK || comando == -1)
                    recibido[ganador] = 1;
           
            // Comando proveniente del jugador perdedor
            if (FD_ISSET(s_jugador[perdedor], &rset))
                if ((comando = procesar_comando(s_jugador[perdedor])) == COM_OK || comando == -1)
                    recibido[perdedor] = 1;
        } else {
            /* Incrementar el número de intentos y volver a enviar END */
            num_intentos++;
            
            // Enviar comando END W al ganador (si procede))
            if (!recibido[ganador] && write(s_jugador[ganador], "END W", strlen("END W")) < strlen("END W"))
                perror("Error en la comunicación con el jugador 1");
            trace("Enviado: END W\n");

            // Enviar comando END L al perdedor (si procede))
            if (!recibido[perdedor] && write(s_jugador[perdedor], "END L", strlen("END L")) < strlen("END L"))
                perror("Error en la comunicación con el jugador 2");
            trace("Enviado: END L\n");
            
            timer.tv_sec = 0;
            timer.tv_usec = END_RETRY; // Tiempo hasta el próximo intento
        }
    }
    
}